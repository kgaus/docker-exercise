# webapp-color

## build and run docker image

Build image
```
docker build -t webapp-color:lite .
```

Run an instance of the image webapp-color and publish port 8080 on the container to 8282 on the host
```
docker run -d -p 8282:8080 webapp-color:lite
```

## push to registry

Log into registry via
```
docker login <REGISTRY-URL>
```
and provide login information.

Push the image to the registry via
```
docker tag webapp-color:lite <REGISTRY-URL>/<NAME>/webapp-color:lite
docker push <REGISTRY-URL>/<NAME>/webapp-color:lite
```

## pull from registry