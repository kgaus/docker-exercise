# webapp-color

## build and run docker image

Build image
```
docker build -t webapp-color .
```

Run an instance of the image webapp-color and publish port 8080 on the container to 8282 on the host
```
docker run -p 8282:8080 webapp-color
```

## push to registry

Log into registry via
```
docker login <REGISTRY-URL>
```
and provide login information.

Push the image to the registry via
```
docker tag webapp-color <REGISTRY-URL>/<NAME>/webapp-color
docker push <REGISTRY-URL>/<NAME>/webapp-color
```

## pull from registry